'use strict'

const EC     = require('elliptic').ec,
      ec     = new EC('secp256k1'),
      sha256 = require('js-sha256'),
      hex    = require('./hex')

let isMining            = false,
    sid,
    sk, pk,
    difficultyFactor,
    unvalidatedTransactions,
    prevBlockHash,
    currentHeight,
    lastMinerStatusSent = new Date().getTime() - 5000

function createBlock(transactions, nonce) {
  const returnObj = {},
        content   = {}

  content.hashPrevBlock = prevBlockHash
  content.timeStamp     = new Date().getTime()
  content.transactions  = transactions
  content.nonce         = nonce
  content.sid           = sid
  content.height        = currentHeight + 1
  content.pk            = pk

  returnObj.content   = content
  returnObj.signature = sk.sign(JSON.stringify(content)).toDER('hex')
  returnObj.id        = sha256(JSON.stringify(returnObj))

  return returnObj
}

function newBlockFound(block) {
  block.content.transactions.forEach(function (transaction) {
    delete transaction.block
    const index = unvalidatedTransactions.indexOf(transaction)
    if (index >= 0) {
      unvalidatedTransactions.splice(index, 1)
    }
  })

  prevBlockHash = block.id
  currentHeight = block.content.height

  mine()
}

function mine() {
  isMining = true

  let nonce        = 0,
      transactions = unvalidatedTransactions ? unvalidatedTransactions.slice(0, 2000) : [],
      block        = createBlock(transactions, nonce),
      zeroString   = new Array(difficultyFactor + 1).join(0) // String consisting of zeroes

  while (hex.hexToBinary(block.id).slice(0, difficultyFactor) !== zeroString) {
    if ((new Date().getTime() - lastMinerStatusSent) > 5000) { // Every 5 second
      process.send({
        status: {
          nonce: nonce,
          workingHeight: currentHeight + 1,
          prevBlockHash: prevBlockHash,
          timestamp: new Date().getTime()
        }
      })
      lastMinerStatusSent = new Date().getTime()
    }

    block = createBlock(transactions, nonce)
    nonce++
  }

  isMining = false

  //console.log(createDateString(), '[MINER]', 'Found block with height', block.content.height)
  process.send({block: block})

  //newBlockFound(block)
}

function updateVariables(m) {
  sid                     = m.sid
  sk                      = ec.keyFromPrivate(m.sk, 'hex')
  pk                      = m.pk
  difficultyFactor        = m.difficultyFactor
  unvalidatedTransactions = m.unvalidatedTransactions
  prevBlockHash           = m.prevBlockHash
  currentHeight           = m.currentHeight
}

function createDateString() {
  return '[' + new Date().toString() + ']'
}

module.exports = {
  mine,
  createBlock,
  updateVariables,
  newBlockFound
}

// -- IPC -- //
process.on('message', (m) => {
  //console.log(createDateString(), '[MINER]', 'Received message from miner controller', m.type)

  updateVariables(m)
  mine()
})

process.on('exit', function () {
  console.log(createDateString(), '[MINER] killed - exit')
})

process.on('SIGINT', function () { //catches ctrl+c event
  console.log(createDateString(), '[MINER] - SIGINT')
  process.exit(0)
})