'use strict'

const cp       = require('child_process'),
      cpulimit = require('cpulimit')

function MinerController() {
  let transactionAdded = false,
      variables,
      miner

  let options = {
    limit: 80,
    includeChildren: true,
    pid: 0
  }

  function interrupt() {
    if (miner) miner.kill()
  }

  function newBlockFound(block) {
    block.content.transactions.forEach(function (transaction) {
      const index = variables.unvalidatedTransactions.indexOf(transaction)
      if (index >= 0) {
        variables.unvalidatedTransactions.splice(index, 1)
      }
    })

    variables.prevBlockHash = block.id
    variables.currentHeight = block.content.height

    //if (transactionAdded) {
    miner.kill()
    //startMiner(variables)
    //transactionAdded = false
    //}
  }

  function newBlockValidated(block) {
    newBlockFound(block)
    miner.kill()
    startMiner(variables)
  }

  function onMinerResponse(m) {
    if (m.block) {
      //newBlockFound(m.block)
      process.send(m)
      //console.log(createDateString(), '[MINER-CTRL]', 'Received block from miner with height', m.block.content.height)

    } else if (m.status) {
      process.send(m)
      //console.log(createDateString(), '[MINER-CTRL]', 'Received status from miner, working on nonce', m.status.nonce)
    }
  }

  function startMiner(m) {
    if (miner) miner.kill()

    miner     = undefined
    variables = m
    miner     = cp.fork(__dirname + '/miner.js')

    options.pid   = miner.pid
    options.limit = m.limit ? m.limit : 80

    cpulimit.createProcessFamily(options, function (err, processFamily) {
      if (err) {
        console.error(err)
        return
      }
      cpulimit.limit(processFamily, options, function (err) {
        if (err) {
          console.log(err)
        }
      })
    })

    miner.send(variables)

    miner.on('message', onMinerResponse)
  }

  function addTransaction(transaction) {
    if (variables.unvalidatedTransactions.indexOf(transaction) < 0) {
      variables.unvalidatedTransactions.push(transaction)
      transactionAdded = true
    }
  }

  function killController(message) {
    console.log('[MINER-CTRL] Killed -', message)
    if (miner) miner.kill()
  }

  // -- Public methods -- //
  this.addTransaction    = addTransaction
  this.newBlockValidated = newBlockValidated
  this.interrupt         = interrupt
  this.startMiner        = startMiner

  process.on('exit', function () {
    killController('Bye bye')
  })

  process.on('SIGINT', function () {
    console.log(createDateString(), '[MINER-CTRL] SIGINT');
    process.exit(0)
  })

  process.on('SIGTERM', function () {
    console.log(createDateString(), '[MINER-CTRL] SIGTERM');
    process.exit(0)
  })

  process.on('uncaughtException', function (err) {
    console.log(createDateString(), '[MINER-CTRL] Caught exception: ' + err);
  })
}

function createDateString() {
  return '[' + new Date().toString() + ']'
}

module.exports = MinerController

// -- IPC -- //
let minerController
process.on('message', (m) => {
  if (!minerController) minerController = new MinerController()

  // Process message
  switch (m.type) {
    case 'mine':
      minerController.startMiner(m)
      break

    case 'blockValidated':
      minerController.startMiner(m)
      break

    /*case 'transactionValidated':
     minerController.addTransaction(m.transaction)
     break*/

    case 'stop':
      minerController.interrupt()
      break

    default:
      process.send({type: 'INVALID_MESSAGE', content: m})
  }

  //console.log(createDateString(), '[MINER-CTRL]', 'Received message from peer', m.type)
})


