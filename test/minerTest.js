const expect          = require("chai").expect,
      MinerController = require('../main'),
      sha256          = require('js-sha256'),
      EC              = require('elliptic').ec,
      ec              = new EC('secp256k1'),
      Miner           = require('../miner'),
      cp              = require('child_process')


describe("Miner", function () {
  const sk = '3feacf122fd25d5d94383f49d0fa896d37be2d7ce789f3c89376183cf5747253',
        pk = '040a4dafdf4ff9f908e45bfd4a2f622c92fb690d9e0601c5abcda59b4e3431802f34fffc5ba3bf82a652342bba656076943627b1b2d76619420074f6cf7ac536e6'

  let minerController,
      options = {
        sid: sha256(pk),
        sk: sk,
        pk: pk,
        difficultyFactor: 1,
        unvalidatedTransactions: [],
        prevBlockHash: sha256('random'),
        currentHeight: 0
      }

  beforeEach(function () {
    minerController = new MinerController()
    Miner.updateVariables(options)
  })

  afterEach(function () {
    minerController.interrupt()
  })

  it("Can create block", function () {
    const block = Miner.createBlock([], 0)
    expect(block).to.have.property('id')
    expect(block.content).to.have.property('hashPrevBlock')
    expect(block.content).to.have.property('timeStamp')
    expect(block.content).to.have.property('transactions')
    expect(block.content).to.have.property('sid')
    expect(block.content).to.have.property('nonce')
    expect(block.content).to.have.property('height')
    expect(block).to.have.property('content')
    expect(block).to.have.property('signature')
  })

  it("Can mine block", function (done) {
    const miner = cp.fork('./main.js')
    miner.on('message', function (m) {
      if (!m.block) return
      miner.send({type: 'stop'})
      expect(m.block).to.not.be.undefined
      done()
    })

    let options = {
      type: 'mine',
      sid: 1234567,
      sk: sk,
      pk: pk,
      difficultyFactor: 4,
      transactions: [],
      prevBlockHash: 'null',
      currentHeight: 0
    }

    miner.send(options)
  })
})


