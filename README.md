Avery-miner
=========

Basic implementation of an Avery miner which shows and documents IPC calls and structure of a block. 
Can be used as a starting point for other implementations.

## Installation

In the command below replace '*<version_no>*' with eg. '*0.0.1*'

```bash
npm install git+ssh://git@bitbucket.org:avery-team/avery-miner.git#v<version_no>
```

## Usage

```js
const Peer = require('Peer')
let peer = new Peer(args)

peer.registerMiner('avery-miner')
```


## Tests

To run test execute the following command:

```bash 
npm test
```
